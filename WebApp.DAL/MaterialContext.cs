﻿using Microsoft.EntityFrameworkCore;
using WebApp.DAL.Models;


namespace WebApp.DAL
{
    public sealed class MaterialContext : DbContext
    {
        public MaterialContext(DbContextOptions<MaterialContext> options)
            : base(options)
        {
            //Database.EnsureDeleted();
            Database.EnsureCreated();
        
        }
        public DbSet<Material> Materials { get; set; }
        public DbSet<Version> Versions { get; set; }
    }
}
