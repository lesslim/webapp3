﻿using System;
using System.Collections.Generic;

namespace WebApp.DAL.Models
{
    public enum Category
    {
        None,
        Presentation,
        Application,
        Other
    }
    public class Material
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public Category Category { get; set; }
        public ICollection<Version> Versions { get; set; }
    }
}
