﻿using System;

namespace WebApp.DAL.Models
{
    public class Version
    {
        public Guid Id { get; set; }
        public long Size { get; set; }
        public DateTime UploadDateTime { get; set; }
        public int Release { get; set; }
        public string Path { get; set; }
        public int MaterialId { get; set; }
        public Material Material { get; set; }
    }
}
