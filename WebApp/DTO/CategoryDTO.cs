﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApp.DTO
{
    public enum CategoryDTO
    {
        Presentation = 1,
        Application,
        Other
    }
}
