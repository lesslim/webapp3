﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApp.DTO
{
    public class FileDTO
    {
        public string Name { get; set; }
        public CategoryDTO Category { get; set; }
        public byte[] File { get; set; }
    }
}
