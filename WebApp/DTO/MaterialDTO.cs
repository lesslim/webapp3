﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApp.DAL.Models;


namespace WebApp.DTO
{
    public class MaterialDTO
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public CategoryDTO Category { get; set; }
    }
}
