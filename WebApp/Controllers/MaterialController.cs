﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using WebApp.DAL.Models;
using System;
using System.Collections.Generic;
using System.IO;
using AutoMapper;
using WebApp.Interfaces;
using Microsoft.Extensions.Logging;
using WebApp.BusinessModels;
using WebApp.DTO;

namespace WebApp.Controllers
{


    [Route("api/[controller]")]
    [ApiController]
    public class MaterialController : Controller
    {
        private readonly IMaterialsService _services;
        private readonly ILogger<MaterialController> _logger;
        private readonly IMapper _mapper;

        public MaterialController(IMaterialsService services, ILogger<MaterialController> logger, IMapper mapper)
        {
            _services = services;
            _logger = logger;
            _mapper = mapper;
        }

        private static string TimeNow()
        {
            return DateTime.Now.ToShortDateString() + ", " + DateTime.Now.ToLongTimeString() + " ";
        }

        [Route("Upload")]
        [HttpPost]
        public async Task<IActionResult> UploadFile(IFormFile upload, CategoryDTO category)
        {
            _logger.LogInformation(TimeNow() + "File uploading.");
            if ((int)category < 1 || (int)category > 3)
                return BadRequest("Invalid category.");
            if (upload == null)
                return BadRequest("No file.");
            if (upload.Length > 2147483648)
                return BadRequest("File size exceeds 2 GB.");

            try
            {
                byte[] file;
                await using (var memoryStream = new MemoryStream())
                {
                    await upload.CopyToAsync(memoryStream);
                    file = memoryStream.ToArray();
                }

                var material = new FileDTO
                {
                    Name = upload.FileName,
                    Category = category,
                    File = file
                };


                var ans = await _services.UploadFileAsync(material);
                _logger.LogInformation(TimeNow() + "Successful uploading.");
                return Ok(_mapper.Map<MaterialBM, MaterialDTO>(ans));
            }
            catch (Exception ex)
            {
                _logger.LogInformation(TimeNow() + "Fail uploading.");
                return BadRequest($"Error: {ex.Message}");
            }
        }

        [Route("Category")]
        [HttpGet]
        public IActionResult GetByCategory(CategoryDTO? category)
        {
            if ((category == null) || ((int)category >= 1 && (int)category <= 3))
            {
                var materials = _services.GetByCategory(category);
                _logger.LogInformation(TimeNow() + "Get all files.");
                return Ok(_mapper.Map<IEnumerable<MaterialBM>, List<MaterialDTO>>(materials));
            }
            else
                return BadRequest("Wrong category.");
        }


        [HttpGet("Name")]
        public IActionResult GetByName(string name)
        {
            var material = _services.GetByName(name);
            if (material == null)
            {
                return NotFound("The file with name " + name + " was not found.");
            }
            _logger.LogInformation(TimeNow() + "Get file " + name);
            return Ok(_mapper.Map<IEnumerable<MaterialBM>, List<MaterialDTO>>(material));
        }

        [Route("Download")]
        [HttpGet]
        public async Task<IActionResult> DownloadMaterial(string name, CategoryDTO category, int? version)
        {
            if ((int)category < 1 || (int)category > 3)
                return BadRequest("Invalid category.");
            var file = await _services.DownloadMaterial(name, (int)category, version);
            if (file == null)
                return BadRequest("File not found.");
            try
            {
                _logger.LogInformation(TimeNow() + "File " + name + " was downloaded.");
                return File(file, "application/octet-stream", name);
            }
            catch (Exception e)
            {
                return BadRequest($"Something wrong: {e.Message}");
            }

        }

        [Route("Category")]
        [HttpPatch]
        public ActionResult<MaterialDTO> ChangeCategory(string name, CategoryDTO oldCategory, CategoryDTO newCategory)
        {
            var material = _services.ChangeCategory(name, oldCategory, newCategory);
            if (material == null)
                return BadRequest();
            _logger.LogInformation(TimeNow() + "Category was changed");
            return Ok(_mapper.Map<MaterialBM, MaterialDTO>(material));
        }

        [HttpDelete]
        [Route("{name}")]
        public ActionResult<MaterialDTO> DeleteMaterial(string name, CategoryDTO category)
        {
            var material = _services.DeleteMaterial(name, category);
            if (material == null)
            {
                _logger.LogInformation(TimeNow() + "Fail " + name + " deletion.");
                return NotFound($"Can't found {name}");
            }
            _logger.LogInformation(TimeNow() + "File " + name + " was deleted.");
            return Ok(_mapper.Map<MaterialBM, MaterialDTO>(material));
        }
    }
}