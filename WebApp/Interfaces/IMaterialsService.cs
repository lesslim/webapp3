﻿using System.Collections.Generic;
using System.Threading.Tasks;
using WebApp.DAL.Models;
using Microsoft.AspNetCore.Http;
using WebApp.BusinessModels;
using WebApp.DTO;

namespace WebApp.Interfaces
{
    public interface IMaterialsService
    {
        Task<MaterialBM> UploadFileAsync(FileDTO material);
        IEnumerable<MaterialBM> GetByCategory(CategoryDTO? category);
        IEnumerable<MaterialBM> GetByName(string name);
        Task<byte[]> DownloadMaterial(string name, int category, int? version);
        MaterialBM ChangeCategory(string name, CategoryDTO oldCategory, CategoryDTO newCategory);
        MaterialBM DeleteMaterial(string name, CategoryDTO category);
    }
}