﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApp.DAL.Models;
using Microsoft.EntityFrameworkCore;
using System.IO;
using System.Text;
using Microsoft.Extensions.Configuration;
using AutoMapper;
using WebApp.Interfaces;
using WebApp.BusinessModels;
using WebApp.DAL;
using WebApp.DTO;

namespace WebApp.Services
{
    public class MaterialsService : IMaterialsService
    {
        private readonly MaterialContext _context;
        private readonly IConfiguration _configuration;
        private readonly IMapper _mapper;

        public MaterialsService(MaterialContext context, IConfiguration configuration, IMapper mapper)
        {
            _context = context;
            _configuration = configuration;
            _mapper = mapper;
        }
        public async Task<MaterialBM> UploadFileAsync(FileDTO upload)
        {
            var isNew = false;
            var same = _context.Materials.Include(x => x.Versions).FirstOrDefault(p => p.Name == upload.Name && p.Category == (Category) upload.Category);
            
            var release = 1;
            if (same != null)
                release = same.Versions.OrderBy(x => x.Release).Last().Release + 1;
            else
            {
                isNew = true;
                same = new Material {Category = (Category) upload.Category, Name = upload.Name};
                await _context.Materials.AddAsync(same);
            }
            var path = $"{_configuration.GetValue<string>("FilesPath")}/{upload.Category}_{release}_{upload.Name}";

            var version = new DAL.Models.Version
            {
                Material = same,
                Path = path,
                Release = release,
                Size = upload.File.Length,
                UploadDateTime = DateTime.Now,
            };

            await _context.Versions.AddAsync(version);

            try
            {
                await _context.SaveChangesAsync();
                if (!Directory.Exists(_configuration.GetValue<string>("FilesPath")))
                {
                    Directory.CreateDirectory(_configuration.GetValue<string>("FilesPath"));
                }

                await File.WriteAllBytesAsync(path, upload.File);
            }
            catch (DbUpdateException db)
            {
                throw db;
            }
            catch (Exception ex)
            {
                if (isNew)
                {
                    _context.Remove(same);
                }
                _context.Versions.Remove(version);
                throw ex;
            }
            return _mapper.Map<Material, MaterialBM>(same);
        }

        public IEnumerable<MaterialBM> GetByCategory(CategoryDTO? category)
        {
            IEnumerable<Material> materials = _context.Materials;

            if (category != null)
                materials = materials.Where(m => m.Category == (Category) category).ToList();

            return _mapper.Map<IEnumerable<Material>, List<MaterialBM>>(materials);
        }

        public IEnumerable<MaterialBM> GetByName(string name)
        {
            IEnumerable<Material> materials = _context.Materials;

            materials = materials.Where(m => m.Name == name).ToList();
            
            return _mapper.Map<IEnumerable<Material>, List<MaterialBM>>(materials);
        }

        public async Task<byte[]> DownloadMaterial(string name, int category, int? release)
        {
            byte[] file = null;
            var material = await _context.Materials.Include(p => p.Versions).FirstOrDefaultAsync(p => (p.Name == name && p.Category == (Category)category));
            if (material == null) return file;
            var version = material.Versions.OrderBy(x => x.Release).LastOrDefault();
            if (release != null)
                version = material.Versions.FirstOrDefault(x => x.Release == release);

            file = await File.ReadAllBytesAsync(version.Path);
            return file;
        }

        public MaterialBM ChangeCategory(string name, CategoryDTO oldCategory, CategoryDTO newCategory)
        {
            var material = _context.Materials.Include(x => x.Versions).FirstOrDefault(p => (p.Name == name && p.Category == (Category)oldCategory));
            if (material == null) return null;
            foreach (var v in material.Versions)
            {
                var oldPath = v.Path;
                var newPath = $"{_configuration.GetValue<string>("FilesPath")}/{newCategory}_{v.Release}_{name}";
                File.Move(oldPath, newPath, true);
                v.Path = newPath;
            }
            material.Category = (Category) newCategory;

            try
            {
                _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return _mapper.Map<Material, MaterialBM>(material);

        }

        public MaterialBM DeleteMaterial(string name, CategoryDTO category)
        {
            var material = _context.Materials.Include(p => p.Versions).FirstOrDefault(p => (p.Name == name && p.Category == (Category)category));
            if (material == null)
                return null;

            _context.Materials.Remove(material);

            try
            {
                _context.SaveChangesAsync();
                foreach (var ver in material.Versions)
                {
                    File.Delete(ver.Path);
                }
            }
            catch (Exception db)
            {
                throw db;
            }
            return _mapper.Map<Material, MaterialBM>(material);
        }
    }
}
