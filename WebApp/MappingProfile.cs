﻿using WebApp.DAL.Models;
using WebApp.BusinessModels;
using AutoMapper;
using Version = WebApp.DAL.Models.Version;
using WebApp.DTO;

namespace WebApp
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<Material, MaterialBM>();
            CreateMap<Version, VersionBM>();
            CreateMap<MaterialBM, MaterialDTO>();
        }
    }
}
