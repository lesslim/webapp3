﻿using System;
using System.Collections.Generic;

namespace WebApp.BusinessModels
{
    public enum CategoryBM
    {
        None,
        Presentation,
        Application,
        Other
    }
    public class MaterialBM
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public CategoryBM Category { get; set; }
        public ICollection<VersionBM> Versions { get; set; }
    }
}
